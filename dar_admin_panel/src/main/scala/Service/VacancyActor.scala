package Service

import DB.{PostgreComponent, VacancyRepository}
import Models._
import Routing.{ApiMessage }
import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.pattern.pipe
import com.typesafe.config.ConfigFactory
import VacancyActor._

import scala.concurrent.Future

/**
  * Created by diana on 8/21/17.
  */
object VacancyActor {
  case class GetVacancy() extends ApiMessage
  case class DeleteVacancy(id:Int) extends ApiMessage
  case class UpdateVac(id:Option[Int], name:String, responsibility:String, demands:String, conditions:String) extends ApiMessage
  case class CreateVacancy(id:Option[Int], name:String, responsibility:String, demands:String, conditions:String) extends ApiMessage
}

class VacancyActor extends Actor with ActorLogging {

  val config = ConfigFactory.load()
  implicit val system = ActorSystem()
  implicit val executionContext = system.dispatcher


  object vacancyRepository extends VacancyRepository with PostgreComponent


    def receive = {

      case m: CreateVacancy =>
        val vac: Vacancy = Vacancy(m.id, m.name, m.responsibility, m.demands, m.conditions)
        vacancyRepository.create(vac)
        val res = vacancyRepository.getAll().pipeTo(self)
        res.pipeTo(self)

      case m: GetVacancy =>
        val allVac = vacancyRepository.getAll()
        allVac.pipeTo(self)


      case m: DeleteVacancy =>
        vacancyRepository.delete(m.id)
        val delVac = vacancyRepository.getAll().pipeTo(self)
        log.info(s"DELETE $delVac")

      case m: UpdateVac =>

        val upVac = Vacancy(m.id, m.name, m.responsibility, m.demands, m.conditions)
        vacancyRepository.update(upVac)
        val res = vacancyRepository.getAll().pipeTo(self)
        res.pipeTo(self)


      case m: ResultOfCreate =>
        log.info(s"Create res $m")
        context.parent ! m

      case m: ResultOfGet =>
        context.parent ! m

      case m: ResultOfDelete =>
        log.info(s"Delete res $m")
        context.parent ! m

      case m: ResultOfUpdate =>
        log.info(s"Update res $m")
         context.parent ! m

      case m =>
        log.info(s"Invalid message: $m")

    }
  }
