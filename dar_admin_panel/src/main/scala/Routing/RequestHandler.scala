package Routing
import Models._
import akka.http.scaladsl.marshalling._
import com.mongodb.casbah.commons.conversions.scala.Serializers
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.http.scaladsl.server.{RequestContext, Route, RouteResult}
import RequestHandler._
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.util.Timeout
import VacancyJsonSupport._
import scala.concurrent.{Future}

import scala.concurrent.duration.{Duration, _}

import StatusCodes._

/**
  * Created by diana on 9/10/17.
  */


import scala.concurrent.{Promise}

trait ApiMessage extends Serializable
import scala.concurrent.ExecutionContext.Implicits.global

object RequestHandler {

  // an imperative wrapper for request context
  final class ImperativeRequestContext(ctx: RequestContext, promise: Promise[RouteResult]) {
    val requestContext: RequestContext = ctx
    private implicit val ec = ctx.executionContext

    def complete(obj: ToResponseMarshallable): Unit = ctx.complete(obj).onComplete(promise.complete)

    def fail(error: Throwable): Unit = ctx.fail(error).onComplete(promise.complete)
  }

  case class HandleActor(target: ActorRef, message: ApiMessage)

  case class HandleProps(props: Props, message: ApiMessage)

  def imperativelyComplete(inner: ImperativeRequestContext => Unit): Route = {
    ctx: RequestContext =>
      val p = Promise[RouteResult]()
      inner(new ImperativeRequestContext(ctx, p))
      p.future
  }
 }



class RequestHandler(ctx: ImperativeRequestContext) extends Actor with Serializers
  with GenericMarshallers
  with PredefinedToEntityMarshallers
  with PredefinedToResponseMarshallers
  with PredefinedToRequestMarshallers
{

  import RequestHandler._

  implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()

    implicit val timeout: Timeout = 5.seconds

    def receive: Actor.Receive = {

      case HandleActor(target, message) =>
        log.info(s"something $message")
        target ! message

      case HandleProps(props, message) =>
        log.info(s"something $message")

        context.actorOf(props) ! message


      case mes: ResultOfGet             =>   complete(marshall(OK, mes.result))
    }

  def marshall(statusCode: StatusCode, value: Seq[Vacancy]): Future[HttpResponse] = {
    Marshal(value).to[HttpResponse].map(k => HttpResponse(statusCode, k.headers, k.entity, k.protocol))
    }

  def marshall(statusCode: StatusCode, value: Vacancy): Future[HttpResponse] = {
    Marshal(value).to[HttpResponse].map(k => HttpResponse(statusCode, k.headers, k.entity, k.protocol))
  }


    def complete[T <: AnyRef](obj: ToResponseMarshallable) = {
      ctx.complete(obj)
      context.stop(self)
    }

}

  trait RequestHandlerMaker{

    val system: ActorSystem

    def handle(r: ImperativeRequestContext, target: ActorRef, message: ApiMessage) =
      system.actorOf(Props(new RequestHandler(r))) ! HandleActor(target, message)

    def handle(r: ImperativeRequestContext, props: Props,  message: ApiMessage) =

      system.actorOf(Props(new RequestHandler(r))) ! HandleProps(props, message)
}