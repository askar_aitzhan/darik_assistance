package Routing

import Service.VacancyActor
import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import akka.util.Timeout
import Service.VacancyActor._
import akka.NotUsed
import akka.http.scaladsl.model.headers.LinkParams.title
import akka.stream.scaladsl.{RunnableGraph, Sink, Source}
import com.mongodb.casbah.commons.conversions.scala.Serializers

import scala.concurrent.duration.{Duration, _}


/**
  * Created by diana on 8/21/17.A
  */

object Routing extends  RequestHandlerMaker with Serializers {


  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  implicit val timeout: Timeout = 5.seconds

  val vacancyActor = Props(new VacancyActor())


  val route = pathPrefix("vacancy") {
    import VacancyJsonSupport._

    post {
      entity(as[CreateVacancy]) { mes =>
        RequestHandler.imperativelyComplete { ctx =>
          handle(ctx, vacancyActor, mes)

          //          mes.id match {
          //            case Some(value) =>
          //              handle(ctx, vacancyActor, mes)
          //
          //            case None =>
          //              val error = Error("400", "Bad request can not be empty")
          //              ctx.complete(error)
          //    }


        }
      }
    } ~
      get {
        RequestHandler.imperativelyComplete { ctx =>
          handle(ctx, vacancyActor, VacancyActor.GetVacancy())
        }
      } ~
      delete {
        path(Segment) { (id) =>
          RequestHandler.imperativelyComplete { ctx =>
            handle(ctx, vacancyActor, VacancyActor.DeleteVacancy(id.toInt))
          }
        }
      } ~
      put {
        entity(as[UpdateVac]) { mes =>
          RequestHandler.imperativelyComplete { ctx =>
            handle(ctx, vacancyActor, mes)
          }
        }
      }
  }
}

