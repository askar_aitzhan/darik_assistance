package Routing

import Models.{Error, ResDeleteVacancy, ResGetVacancy, ResultOfCreate, ResultOfGet, Vacancies, Vacancy}
import Service.VacancyActor.{CreateVacancy, GetVacancy, UpdateVac}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol


/**
  * Created by diana on 8/21/17.
  */
object VacancyJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val VacancyFormats = jsonFormat5(Vacancy)
  implicit val VacanciesFormats = jsonFormat1(Vacancies)
  implicit val ErrorFormats=jsonFormat2(Error)
  implicit val createVacancyFormats = jsonFormat5(CreateVacancy)
  implicit val getVacancyFormats =jsonFormat0(GetVacancy)
  implicit val updateVacFormats =jsonFormat5(UpdateVac)
  implicit val ResGetVacancyFormats = jsonFormat1(ResGetVacancy)
  implicit val   ResDeleteVacancyFormats = jsonFormat1(ResDeleteVacancy)

  implicit val ResultOfGetFormats =jsonFormat1(ResultOfGet)
  implicit val resultOfCreateFormats = jsonFormat1(ResultOfCreate)
}
