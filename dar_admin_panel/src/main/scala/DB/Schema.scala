package DB

import Models.Vacancy

/**
  * Created by diana on 9/8/17.
  */
trait Schema { this:DBComponent=>
  import driver.api._

  class Vacancies(tag: Tag) extends Table[Vacancy](tag, "vacancy") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc,  O.Default(0))
    def name = column[String]("name")
    def responsibility = column[String]("responsibility")
    def demands = column[String]("demands")
    def conditions = column[String]("conditions")

    def * = (id.?, name, responsibility, demands, conditions) <> (Vacancy.tupled, Vacancy.unapply)
  }

  lazy val vacancies = TableQuery[Vacancies]
}
