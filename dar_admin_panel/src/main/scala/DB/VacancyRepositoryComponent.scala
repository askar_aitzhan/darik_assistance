package DB

import Models._

import scala.concurrent.Future
/**
  * Created by diana on 8/31/17.
  */
trait VacancyRepositoryComponent { this:DBComponent=>
  def create(vacancy:Vacancy):Future[ResultOfCreate]
  def getAll():Future[ResultOfGet]
  def update(vacancy: Vacancy):Future[ResultOfUpdate]
  def delete(id:Int):Future[ResultOfDelete]
}