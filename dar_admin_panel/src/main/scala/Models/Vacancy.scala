package Models

import Routing.ApiMessage

import scala.concurrent.Future

/**
  * Created by diana on 8/21/17.
  */
case class Vacancy(id:Option[Int],
                   name: String,
                   responsibility:String,
                   demands:String,
                   conditions:String){
  //require(!id.isEmpty, "vacancy id must not be empty")
}

case class Vacancies(vacancies:Seq[Vacancy])

case class Error(code:String, message:String)

case class ResultOfDelete(result: Int)

case class ResultOfUpdate(result:Int)

case class ResultOfCreate(result: Int)

case class ResultOfGet(result: Seq[Vacancy])

