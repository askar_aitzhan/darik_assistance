package Routing

import Models.{Error, News, ResultOfCreate, ResultOfGet}
import Service.NewsActor._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol


/**
  * Created by diana on 8/21/17.
  */
object NewsJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val NewsFormats = jsonFormat5(News)
  //implicit val VacanciesFormats = jsonFormat1(Vacancies)
  implicit val ErrorFormats=jsonFormat2(Error)
  implicit val createNewsFormats = jsonFormat5(CreateNews)
  implicit val getNewsFormats =jsonFormat0(GetNews)
  implicit val updateNewsFormats =jsonFormat5(UpdateNews)

//  implicit val ResGetVacancyFormats = jsonFormat1(ResGetVacancy)
//  implicit val   ResDeleteVacancyFormats = jsonFormat1(ResDeleteVacancy)

  implicit val ResultOfGetFormats =jsonFormat1(ResultOfGet)
  implicit val resultOfCreateFormats = jsonFormat1(ResultOfCreate)
}
