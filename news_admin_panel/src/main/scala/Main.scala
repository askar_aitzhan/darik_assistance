import Service.NewsActor$
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import com.typesafe.config.ConfigFactory

import scala.concurrent.Future
import Routing.{RequestHandlerMaker, Routing}

/**
  * Created by diana on 9/8/17.
  */


object Main extends App with RequestHandlerMaker{
  val sysName="dar-kz-admin-panel"

  implicit val system = ActorSystem(sysName)
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  val config = ConfigFactory.load()

  system.registerOnTermination {
    system.log.info(s"news_admin_panel shutdown.")
  }

  val host = config.getString("application.server.host")
  val port = config.getInt("application.server.port")

  //  IO(Http) ! Http().bindAndHandle(route, "localhost", 8080)
  system.log.info(s"news_admin_panel started: ${host}:${port}")

  val serverSource: Source[Http.IncomingConnection, Future[Http.ServerBinding]] =
    Http().bind(interface = host, port = port)

  val binding: Future[Http.ServerBinding] = serverSource.to(Sink.foreach { conn =>
    conn.handleWith(Routing.route)
  }).run()

}