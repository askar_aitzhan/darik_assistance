package DB
import Models._

/**
  * Created by diana on 9/8/17.
  */

trait NewsRepository extends Schema with NewsRepositoryComponent { this:DBComponent=>
  import driver.api._
  import scala.concurrent.ExecutionContext.Implicits.global

  override def create(news: News) = {

    newsTable.filter(_.position < news.position).result

    //println(s"pos $a")

    val createQuery= newsTable+=news


    //val createQuery = vacancies.filter(_.id === vacancy.id).result.head

    db.run(createQuery).map {
      r => ResultOfCreate(r)
    }
  }

  override def getAll() = {
    val getAllQuery = newsTable.result
    db.run(getAllQuery).map{
      r=> ResultOfGet(r)
    }
  }

  override def update(news: News) = {
    val updateQuery = newsTable.filter(_.id === news.id).update(news)
    db.run(updateQuery).map{
      r=> ResultOfUpdate(r)
    }
  }

  override def delete(id: Int) = {
    val deleteQuery =  newsTable.filter(_.id === id).delete
    db.run(deleteQuery).map{
      r=> ResultOfDelete(r)
    }
  }
}
