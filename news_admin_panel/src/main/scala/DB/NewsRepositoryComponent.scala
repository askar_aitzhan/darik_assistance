package DB

import Models._

import scala.concurrent.Future
/**
  * Created by diana on 8/31/17.
  */
trait NewsRepositoryComponent { this:DBComponent=>
  def create(news:News):Future[ResultOfCreate]
  def getAll():Future[ResultOfGet]
  def update(news: News):Future[ResultOfUpdate]
  def delete(id:Int):Future[ResultOfDelete]
}