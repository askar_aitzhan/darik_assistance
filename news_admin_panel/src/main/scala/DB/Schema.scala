package DB

import Models.News

/**
  * Created by diana on 9/8/17.
  */
trait Schema { this:DBComponent=>
  import driver.api._

  class NewsTable(tag: Tag) extends Table[News](tag, "news") {
    def id = column[Int] ("id", O.PrimaryKey, O.AutoInc,  O.Default(0))
    def caption = column[String]("caption")
    def text = column[String]("text")
    def position = column[Int]("position")
    def image = column[String]("image")

    def * = (id.?, caption, text, position, image) <> (News.tupled, News.unapply)
  }

  lazy val newsTable = TableQuery[NewsTable]
}
