package DB

import com.byteslounge.slickrepo.scalaversion.JdbcProfile

/**
  * Created by diana on 8/31/17.
  */
trait DBComponent {
  val driver: JdbcProfile

  import driver.api._

  val db: Database
}