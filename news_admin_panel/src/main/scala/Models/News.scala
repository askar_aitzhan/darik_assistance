package Models


/**
  * Created by diana on 8/21/17.
  */
case class News(id:Option[Int],
                caption: String,
                text:String,
                position:Int,
                image:String)
  {
  require(caption.length < 75, "Caption should not exceed 75 symbols")
  require(text.length < 90, "Text should not exceed 90 symbols")

}

//case class Vacancies(vacancies:Seq[News])

case class Error(code:String, message:String)

case class ResultOfDelete(result: Int)

case class ResultOfUpdate(result:Int)

case class ResultOfCreate(result: Int)

case class ResultOfGet(result: Seq[News])

