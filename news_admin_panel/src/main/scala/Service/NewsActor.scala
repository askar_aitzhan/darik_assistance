package Service

import java.io.{BufferedOutputStream, FileOutputStream}
import java.util.Base64

import DB.{NewsRepository, PostgreComponent}
import Models._
import Routing.ApiMessage
import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.pattern.pipe
import com.typesafe.config.ConfigFactory
import NewsActor._

import scala.concurrent.Future

/**
  * Created by diana on 8/21/17.
  */
object NewsActor {
  case class GetNews() extends ApiMessage
  case class DeleteNews(id:Int) extends ApiMessage
  case class UpdateNews(id:Option[Int], caption:String, text:String, position:Int, image:String) extends ApiMessage
  case class CreateNews(id:Option[Int], caption:String, text:String, position:Int, image:String) extends ApiMessage
}

class NewsActor extends Actor with ActorLogging {

  val config = ConfigFactory.load()
  implicit val system = ActorSystem()
  implicit val executionContext = system.dispatcher


  object newsRepository extends NewsRepository with PostgreComponent


    def receive = {

      case m: CreateNews =>
        val cap=m.caption
        val base64ImageString = m.image
        val path = s"/home/diana/Desktop/darrrr/images/$cap.jpeg"

        decoder(base64ImageString, path)

        val news: News = News(m.id, m.caption, m.text, m.position, path)
        newsRepository.create(news)
        val res = newsRepository.getAll().pipeTo(self)
        res.pipeTo(self)

      case m: GetNews =>
        val allNews = newsRepository.getAll()
        allNews.pipeTo(self)


      case m: DeleteNews =>
        newsRepository.delete(m.id)
        val delNews = newsRepository.getAll().pipeTo(self)
        log.info(s"DELETE $delNews")

      case m: UpdateNews =>

        val upNews = News(m.id, m.caption, m.text, m.position, m.image)
        newsRepository.update(upNews)
        val res = newsRepository.getAll().pipeTo(self)
        res.pipeTo(self)


      case m: ResultOfCreate =>
        log.info(s"Create res $m")
        context.parent ! m

      case m: ResultOfGet =>
        context.parent ! m

      case m: ResultOfDelete =>
        log.info(s"Delete res $m")
        context.parent ! m

      case m: ResultOfUpdate =>
        log.info(s"Update res $m")
         context.parent ! m

      case m =>
        log.info(s"Invalid message: $m")

    }


  def decoder(imageString: String, path:String) {
    val imageOutFile = new BufferedOutputStream(new FileOutputStream(path))

    val imageByteArray = Base64.getDecoder().decode(imageString);
    imageOutFile.write(imageByteArray);
    imageOutFile.close()

  }

}
