name := "dar_admin_panel"

version := "1.0"

scalaVersion := "2.12.3"


lazy val akkaVersion = "2.5.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % "2.5.3",
  "com.typesafe.akka" %% "akka-http" % "10.0.9",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.9",

  "org.mongodb.scala" %% "mongo-scala-driver" % "2.1.0",

  "com.zaxxer" % "HikariCP" % "2.4.1",
  "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",


  "com.typesafe.slick" %% "slick" % "3.2.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.0"

)

libraryDependencies += "org.mongodb" %% "casbah" % "3.1.1"
libraryDependencies += "com.byteslounge" %% "slick-repo" % "1.4.3"

