package DB

import Models.Vacancy
import Service.VacancyActor.{CreateVacancy, DeleteVacancy, UpdateVacancy}
import scala.concurrent.Future

/**
  * Created by diana on 8/31/17.
  */

trait VacancyRepositoryComponent{ this:DBComponent=>
  def create(vacancy:Vacancy):Future[Int]
  def getAll():Future[List[Vacancy]]
  def getById(id:Int):Future[Option[Vacancy]]
  def update(vacancy: Vacancy):Future[Int]
  def delete(id:Int):Future[Int]
}

  trait VacancyTable {
    this: DBComponent =>

    import driver.api._

    class VacancyTable(tag: Tag) extends Table[Vacancy](tag, "vacancyoppoppopp") {
      val id = column[Int]("id", O.PrimaryKey, O.AutoInc)
      val name = column[String]("name")
      val responsibility = column[String]("responsibility")
      val demands = column[String]("demands")
      val conditions = column[String]("conditions")

      def * = (id.?, name, responsibility, demands, conditions) <> (Vacancy.tupled, Vacancy.unapply)
    }


  //RepositoryQuery

  lazy val vacancyTableQuery = TableQuery[VacancyTable]

  protected def vacancyTableAutoInc = vacancyTableQuery returning vacancyTableQuery.map(_.id)

  def createQuery(vacancy:Vacancy) = vacancyTableQuery+=vacancy

  def getAllQuery() = vacancyTableQuery.to[List].result

  def getByIdQuery(id:Int) = vacancyTableQuery.filter(_.id === id).result.headOption

  def updateQuery(vacancy: Vacancy)= vacancyTableQuery.filter(_.id === vacancy.id.get).update(vacancy)

  def deleteQuery(id:Int) = vacancyTableQuery.filter(_.id === id).delete

}

abstract class VacancyRepository extends VacancyTable with VacancyRepositoryComponent {this:DBComponent=>

  override def create(vacancy: Vacancy)= {
    db.run(createQuery(vacancy))
  }

  override def getAll: Future[List[Vacancy]] = {
    db.run(getAllQuery)
  }

  override def getById(id: Int): Future[Option[Vacancy]] = {
    db.run(getByIdQuery(id))
  }

  override def update(vacancy: Vacancy)= {
    db.run(updateQuery(vacancy))
  }

  override def delete(id: Int) = {
    db.run(deleteQuery(id))
  }
}


trait PostgreComponent extends DBComponent {
  val driver = slick.driver.PostgresDriver
  import driver.api._
  val db: Database = PostgreDB.connectionPool
}

object PostgreDB {
  import slick.driver.PostgresDriver.api._
  val connectionPool = Database.forConfig("postgreDB")
}


















postgreDB = {
  dataSourceClass = "org.postgresql.ds.PGSimpleDataSource"
  properties = {
    databaseName = "dar"
    user = "postgres"
    password = "123"
    serverName="localhost"
  }
  numThreads = 10
}

























  /*def selectNamesOfBig(): Future[Unit] = {
    val query = vacancies.filter(_.population > 4000000).map(c => (c.name, c.population)).result
    runAndLogResults("All city names with population over 4M", query, query)
  }*/

  
  /*def plainSql(): Future[Unit] = {
    case class MetroSystemWithCity(metroSystemName: String, cityName: String, dailyRidership: Int)

    implicit val getMetroSystemWithCityResult = GetResult(r => MetroSystemWithCity(r.nextString, r.nextString, r.nextInt))

    val query = sql"""SELECT ms.name, c.name, ms.daily_ridership
                             FROM metro_system as ms
                             JOIN city AS c ON ms.city_id = c.id
                             ORDER BY ms.daily_ridership DESC""".as[MetroSystemWithCity]

    runAndLogResults("Plain sql", query)
  }*/
































///

import Models.Vacancy
import slick.jdbc.{JdbcBackend, JdbcProfile}
import slick.sql.SqlAction

import scala.concurrent.{Future}
import scala.concurrent.ExecutionContext.Implicits.global


  trait Schema {
    val jdbcProfile: JdbcProfile

    import jdbcProfile.api._

    class Vacancies(tag: Tag) extends Table[Vacancy](tag, "vacancy") {
      def id = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)

      def name = column[String]("name")

      def responsibility = column[String]("responsibility")

      def demands = column[String]("demands")

      def conditions = column[String]("conditions")

      def * = (id, name, responsibility, demands, conditions) <> (Vacancy.tupled, Vacancy.unapply)
    }

    lazy val vacancies = TableQuery[Vacancies]
  }

  trait VacancyRepository extends Schema {
    val db: JdbcBackend#DatabaseDef
    val jdbcProfile: JdbcProfile

    import jdbcProfile.api._

    def insertWithGeneratedId(): Future[Unit] = {
      val insertQuery = (vacancies returning vacancies.map(_.id)) += Vacancy(Option(1), "New York", "know smth", "work work", "fine")
      db.run(insertQuery).map { r =>
        println(s"Inserted, generated id: $r")
        println()
      }
    }

    def selectAll(): Future[Unit] = {
      val query = vacancies.result
      runAndLogResults("All cities", query, query)
    }

    private def runAndLogResults[R](label: String, sqlQuery: SqlAction[Seq[R], NoStream, _], query: DBIOAction[Seq[R], NoStream, _]): Future[Unit] = {
      db.run(query).map { r =>
        println(label)
        r.foreach(println)
        println("Generated query:")
        println(sqlQuery.statements.mkString("\n"))
        println()
      }
    }

    private def runAndLogResults[R](label: String, query: DBIOAction[Seq[R], NoStream, _]): Future[Unit] = {
      db.run(query).map { r =>
        println(label)
        r.foreach(println)
        println()
      }
    }
  }






























  //  val db = config.getString("application.database.db")
  // val host = config.getString("application.database.host")
  // val collectionName = config.getString("application.database.collection")

  //val codecRegistry = fromRegistries(fromProviders(classOf[Vacancy]), DEFAULT_CODEC_REGISTRY )
  //val mongoClient: MongoClient = MongoClient(s"mongodb:// ${host}")
  //val database: MongoDatabase = mongoClient.getDatabase(s"${db}").withCodecRegistry(codecRegistry)
  //val collection: MongoCollection[Vacancy]= database.getCollection(s"${collectionName}")

  // Singleton instance of bank repository for  Production
  //object vacancyRepository extends VacancyRepository with PostgreComponent





















    //  collection.deleteOne(equal("id", id)).toFuture().pipeTo(self)
  //    val delVac = collection.find().toFuture().pipeTo(self)
    //  sender() ! Vacancies(delVac)

    //collection.deleteOne(equal("id", id)).results()
      //val delVac = collection.find().results()
      //sender() ! Vacancies(delVac)

